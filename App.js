class App {
  constructor() {
    this.tasksList = []
    this.currentDay = "today"
    this.currentList = []
    this.allowance = 0
  }

  async init() {
    //Load JSON and set Allowance/Current List
    await this.loadJSON();
    this.setAllowance();
    this.setCurrentList()
    // this.handleAllowanceView();
  }

  async loadJSON() {
    const data = await fetch("./tareas.json")
    const tasksList = await data.json();
    this.tasksList = tasksList
    
  }

  setAllowance() {
    const listsNames = Object.keys(this.tasksList)
    let actualAllowance = 0
    listsNames.forEach(list => {
      this.tasksList[list].forEach(task => task.completed ? actualAllowance += task.points : null)
    })
    this.allowance = actualAllowance
  }

  setCurrentDay(day){
    this.currentDay = day
  }

  setCurrentList(){
    this.currentList = this.tasksList[this.currentDay]
  }

  toggleCompletedTask(taskId){
    const list = this.currentList
    console.log(list);
    console.log(taskId);

    const task = list.find(t => t.id == taskId)
    console.log(task);
    task.completed ? task.completed = 0 : task.completed = 1

    if (!task) return false
  }



}


