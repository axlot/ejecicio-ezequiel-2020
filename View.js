class View {

  constructor(app) {
    this.daysButtons = document.querySelectorAll('[class^="todo__days--"]')
    this.loader = document.querySelector(".lds-circle")
    this.app = app
  }

  async init() {

    await this.app.init();
    this.hideLoader();

    this.updateAllowance()
    this.setCurrentDayListener();
    this.updateCurrentList()
    this.setCheckboxesListeners();
  }

  hideLoader() {
    this.loader.style.display = "none"
  }

  setCurrentDayListener() {
    this.daysButtons.forEach(button => {
      const selectedDay = button.dataset["day"]
      button.addEventListener("click", () => this.handleCurrentDay(selectedDay))
    });
  }

  handleCurrentDay(selectedDay) {
    if(selectedDay != this.app.currentDay) 
    this.updateCurrentDay(selectedDay);
    this.app.setCurrentDay(selectedDay)
    this.app.setCurrentList()
    this.updateCurrentList();
    this.setCheckboxesListeners();
  }

  updateCurrentDay(selectedDay) {
    let prevDay = this.app.currentDay
    let prevButton = document.querySelector(`li[data-day=${prevDay}]`)
    let newButton = document.querySelector(`li[data-day=${selectedDay}]`)

    prevDay === selectedDay
      ? ""
      :// Turn Off previous day selected
      prevButton.classList.remove("todo__days--on")
      prevButton.classList.add("todo__days--off")
      // Turn on new day selected
      newButton.classList.add("todo__days--on")
  }

  updateCurrentList() {
    const list = document.querySelector(".todo__list > ul")
    list.innerHTML = ""

    this.app.currentList.forEach(task => {
      list.innerHTML += this.newTask(task);
    })

  }

  updateAllowance() {
    const allowanceSpan = document.getElementById("allowance")
    allowanceSpan.innerText = `$${this.app.allowance}`
  }

  updateTaskView(taskId) {
    const task = document.getElementById(`t-${taskId}`)
    task.classList.toggle("selected")
    task.children[0].classList.toggle("todo__circle--active")
    task.children[0].classList.toggle("todo__circle--done")
  }

  handleTaskCheck(e) {
    const taskId = e.target.id
    // e.target.checked ? setCompletedTas : task.completed = 0
    this.app.toggleCompletedTask(taskId);
    this.app.setAllowance();
    this.updateAllowance();
    this.updateTaskView(taskId)
  }

  setCheckboxesListeners = () => {

    let checkboxes = document.querySelectorAll('input[type="checkbox"]');

    checkboxes.forEach(checkbox => {
      checkbox.addEventListener('click', (e) => this.handleTaskCheck(e))
    })
  }

  newTask(taskData) {
    const { id, task, category, points, icon, completed } = taskData

    return `
        <li class="todo__item ${completed ? "selected" : ""}" id='t-${id}'>
          <div class="todo__circle--${completed ? "done" : "active"}">
            <img src="./assets/${icon}" alt='${task}' class="todo__list--img">
          </div>  
          <div class="todo__item--description">
            <p class="todo__item--title">${task}</p>
            <span class="todo__item--place">${category}</span>
          </div>
          <label class="todo__list--check">
            <input type="checkbox" name="" id=${id} ${completed ? "checked" : ""}>
              <div class="checkmark"></div>
          </label>
        </li>`
  }
}